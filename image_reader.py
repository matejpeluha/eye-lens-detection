import cv2 as cv
from tkinter import filedialog
from copy import deepcopy
import numpy as np
import csv
import os
import glob


class ImageReader:
    def __init__(self):
        self.equalization = 0
        self.kernel = 3
        self.sigma = 1
        self.median_blur = 1
        self.canny_min = 0
        self.canny_max = 0
        self.canny_on = 0
        self.circles_on = 0
        self.accumulator = 2
        self.min_dist = 6
        self.min_radius = 0
        self.max_radius = 250
        self.param1 = 263
        self.param2 = 104
        self.is_bilateral_on = 0
        self.bilateral_filter_size = 1
        self.bilateral_sigma_color = 0
        self.bilateral_sigma_space = 0
        self.iou_border = 0.75
        self.true_positive = 0
        self.false_positive = 0
        self.false_negative = 2
        self.is_circle1_detected = False
        self.is_circle2_detected = False
        self.best_precision = {'1': 0, '2': 0}
        self.best_recall = {'1': 0, '2': 0}
        self.original_image = None
        self.csv_data = None
        self.IMAGE_WINDOW_NAME = 'Matej Peluha BIOM'
        self.OPTIONS_WINDOW_NAME = 'OPTIONS'

    """
    ////////////////////////////////////////////
    GRID SEARCH
    ////////////////////////////////////////////
    """

    def run_grid_search(self):
        # different csv index
        for csv_index in range(1, 3):
            # settings of gaussian blur
            self.kernel = 1
            self.sigma = 1
            self.accumulator = 2
            self.min_dist = 1
            self.param1 = 100
            self.param2 = 50
            for gaussian_kernel in range(1, 17, 4):
                for gaussian_sigma in range(1, 6, 2):
                    # settings of hough circles
                    for accumulator in range(2, 5):
                        for min_dist in range(1, 31, 10):
                            for param_1 in range(100, 301, 20):
                                for param_2 in range(50, 200, 20):
                                    # try all images
                                    avg_precision = 0
                                    avg_recall = 0
                                    counter = 0
                                    for source in os.listdir('./sources'):
                                        if source == 'iris_annotation.csv':
                                            continue
                                        for direction in ['L', 'R']:
                                            path = './sources/' + source + '/' + direction + '/*.jpg'
                                            for file_path in glob.glob(path):
                                                # try settings on one image
                                                file_path = file_path.replace('\\', '/')
                                                image = cv.imread(file_path)
                                                self.load_csv_from_csv(file_path)
                                                print(file_path, ' ----- csv_index: ', csv_index)
                                                is_circle_detected = False
                                                true_positive = 0
                                                false_positive = 0
                                                false_negative = 1
                                                image = cv.GaussianBlur(image, (gaussian_kernel, gaussian_kernel),
                                                                        gaussian_sigma)
                                                circles = cv.HoughCircles(
                                                    cv.cvtColor(image, cv.COLOR_BGR2GRAY),
                                                    cv.HOUGH_GRADIENT,
                                                    dp=accumulator,
                                                    minDist=min_dist,
                                                    param1=param_1,
                                                    param2=param_2,
                                                    minRadius=0,
                                                    maxRadius=0
                                                )
                                                if circles is None or len(circles) == 0:
                                                    continue
                                                circles = np.int16(np.around(circles))
                                                # result of circles in image
                                                for circle_data in circles[0, :]:
                                                    if self.calculate_iou(circle_data, csv_index) > self.iou_border and is_circle_detected is False:
                                                        true_positive += 1
                                                        false_negative -= 1
                                                        is_circle_detected = True
                                                    else:
                                                        false_positive += 1
                                                # precision and recall
                                                if true_positive + false_positive > 0:
                                                    avg_precision += true_positive / (true_positive + false_positive)
                                                avg_recall += true_positive / (true_positive + false_negative)
                                                counter += 1
                                    # compare with best recall and precision
                                    avg_precision /= counter
                                    avg_recall /= counter
                                    if avg_precision >= self.best_precision.get(str(csv_index)) and avg_recall >= self.best_recall.get(str(csv_index)):
                                        self.best_precision.update({str(csv_index): avg_precision})
                                        self.best_recall.update({str(csv_index): avg_recall})
                                        self.kernel = gaussian_kernel
                                        self.sigma = gaussian_sigma
                                        self.accumulator = accumulator
                                        self.min_dist = min_dist
                                        self.param1 = param_1
                                        self.param2 = param_2
                                    print('BEST PRECISION: ', self.best_precision)
                                    print('BEST RECALL: ', self.best_recall)
            print('BEST PRECISION: ', self.best_precision)
            print('BEST RECALL: ', self.best_recall)
            print('KERNEL: ', self.kernel)
            print('SIGMA: ', self.sigma)
            print('ACCUMULATOR: ', self.accumulator)
            print('MIN DIST: ', self.min_dist)
            print('PARAM 1: ', self.param1)
            print('PARAM 2: ', self.param2)

    """
    ////////////////////////////////////////////
    APP
    ////////////////////////////////////////////
    """

    def run(self):
        """RUN PROGRAM"""
        self.load_image()
        if self.original_image is None:
            return
        self.load_window()
        self.load_options()

        key = cv.waitKey(0) & 0xFF
        if key == 27:
            cv.destroyAllWindows()

    def update_image(self):
        image = self.get_filtered_image()
        cv.imshow(self.IMAGE_WINDOW_NAME, image)

    """IMAGE PROCESS"""

    def get_filtered_image(self):
        image = deepcopy(self.original_image)
        self.clean_stats()
        if self.equalization:
            image = cv.cvtColor(cv.equalizeHist(cv.cvtColor(image, cv.COLOR_BGR2GRAY)), cv.COLOR_GRAY2BGR)
        image = cv.GaussianBlur(image, (self.kernel, self.kernel), self.sigma)
        image = cv.medianBlur(image, ksize=self.median_blur)
        if self.canny_on:
            image = cv.Canny(image, self.canny_min, self.canny_max)
        if self.is_bilateral_on:
            image = cv.bilateralFilter(image, d=self.bilateral_filter_size, sigmaColor=self.bilateral_sigma_color, sigmaSpace=self.bilateral_sigma_space)
        if self.circles_on:
            self.add_circles_to_image(image)
            image = self.add_stats_to_image(image)
        return image

    def clean_stats(self):
        self.true_positive = 0
        self.false_positive = 0
        self.false_negative = 2
        self.is_circle1_detected = False
        self.is_circle2_detected = False

    def add_circles_to_image(self, image):
        if image is None:
            self.draw_csv_circles(image)
            return

        circles = cv.HoughCircles(
            cv.cvtColor(image, cv.COLOR_BGR2GRAY),
            cv.HOUGH_GRADIENT,
            self.accumulator,
            self.min_dist,
            param1=self.param1,
            param2=self.param2,
            minRadius=self.min_radius,
            maxRadius=self.max_radius
        )
        if circles is None or len(circles) == 0:
            self.draw_csv_circles(image)
            return
        circles = np.int16(np.around(circles))
        for circle_data in circles[0, :]:
            self.draw_circle(image, circle_data)

        self.draw_csv_circles(image)

    def draw_circle(self, image, circle_data):
        width = 2
        if self.calculate_iou(circle_data, 1) > self.iou_border and self.is_circle1_detected is False:
            color = (0, 140, 0)
            self.true_positive += 1
            self.false_negative -= 1
            self.is_circle1_detected = True
        elif self.calculate_iou(circle_data, 2) > self.iou_border and self.is_circle2_detected is False:
            color = (0, 110, 0)
            self.true_positive += 1
            self.false_negative -= 1
            self.is_circle2_detected = True
        else:
            color = (0, 0, 255)
            width = 1
            self.false_positive += 1

        cv.circle(image, (circle_data[0], circle_data[1]), circle_data[2], color, width)
        cv.circle(image, (circle_data[0], circle_data[1]), 2, color, width)

    def draw_csv_circles(self, image):
        for i in range(1, 3):
            center_x = int(self.csv_data['center_x_' + str(i)])
            center_y = int(self.csv_data['center_y_' + str(i)])
            radius = int(self.csv_data['polomer_' + str(i)])
            cv.circle(image, (center_x, center_y), radius, (255, 0, 0), 2)
            cv.circle(image, (center_x, center_y), 2, (255, 0, 0), 2)

    def add_stats_to_image(self, image):
        text1 = 'TP: ' + str(self.true_positive) + \
                ' FP: ' + str(self.false_positive) + \
                ' FN: ' + str(self.false_negative)
        if self.true_positive + self.false_positive == 0:
            text2 = 'Precision: 0.0'
        else:
            text2 = 'Precision: ' + str(self.true_positive / (self.true_positive + self.false_positive))
        text3 = 'Recall: ' + str(self.true_positive / (self.true_positive + self.false_negative))

        image = cv.putText(image, text1, (5, 10), cv.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 0))
        image = cv.putText(image, text2, (5, 25), cv.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 0))
        return cv.putText(image, text3, (5, 40), cv.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 0))

    def calculate_iou(self, circle, csv_circle_index):
        """
        Source: https://pyimagesearch.com/2016/11/07/intersection-over-union-iou-for-object-detection/
        :param circle:
        :param csv_circle_index:
        :return iou:
        """
        intersection = self.get_intersection(circle, csv_circle_index)
        union = self.get_union(circle, csv_circle_index, intersection)

        return intersection / union

    def get_intersection(self, circle, csv_circle_index):
        center_x = int(self.csv_data['center_x_' + str(csv_circle_index)])
        center_y = int(self.csv_data['center_y_' + str(csv_circle_index)])
        radius = int(self.csv_data['polomer_' + str(csv_circle_index)])

        xA = max(center_x - radius, circle[0] - circle[2])
        yA = max(center_y - radius, circle[1] - circle[2])
        xB = min(center_x + radius, circle[0] + circle[2])
        yB = min(center_y + radius, circle[1] + circle[2])

        return max(0, xB - xA + 1) * max(0, yB - yA + 1)

    def get_union(self, circle, csv_circle_index, intersection):
        center_x = int(self.csv_data['center_x_' + str(csv_circle_index)])
        center_y = int(self.csv_data['center_y_' + str(csv_circle_index)])
        radius = int(self.csv_data['polomer_' + str(csv_circle_index)])

        boxAArea = ((center_x + radius) - (center_x - radius) + 1) * ((center_y + radius) - (center_y - radius) + 1)
        boxBArea = ((circle[0] + circle[2]) - (circle[0] - circle[2]) + 1) * ((circle[1] + circle[2]) - (circle[1] - circle[2]) + 1)

        return float(boxAArea + boxBArea - intersection)

    """EQUALIZATION"""

    def update_equalization(self, value):
        self.equalization = value / 10
        self.update_image()

    """GAUSSIAN BLUR"""

    def update_kernel(self, value):
        self.kernel = value
        if self.kernel % 2 == 0:
            self.kernel += 1
        self.update_image()

    def update_sigma(self, value):
        self.sigma = value
        if self.sigma == 0:
            self.sigma = 1
        self.update_image()

    """"CANNY EDGES DETECTION"""

    def update_treshold(self, value):
        self.canny_min = value
        self.canny_max = value * 2
        self.update_image()

    def update_canny(self, value):
        self.canny_on = value
        self.update_image()

    """BILATERALL FILTER"""

    def update_is_bilateral_on(self, value):
        self.is_bilateral_on = value
        self.update_image()

    def update_bilateral_filter_size(self, value):
        self.bilateral_filter_size = value
        if self.bilateral_filter_size == 0:
            self.bilateral_filter_size = 1
        self.update_image()

    def update_bilateral_sigma_color(self, value):
        self.bilateral_sigma_color = value
        self.update_image()

    def update_bilateral_sigma_space(self, value):
        self.bilateral_sigma_space = value
        self.update_image()

    """"HOUGH TRANSFORMATION"""

    def update_circles_on(self, value):
        self.circles_on = value
        self.update_image()

    def update_accumulator(self, value):
        self.accumulator = value
        if self.accumulator == 0:
            self.accumulator = 1
        self.update_image()

    def update_min_dist(self, value):
        self.min_dist = value
        if self.min_dist == 0:
            self.min_dist = 1
        self.update_image()

    def update_min_radius(self, value):
        self.min_radius = value
        self.update_image()

    def update_max_radius(self, value):
        self.max_radius = value
        self.update_image()

    def update_param1(self, value):
        self.param1 = value
        if self.param1 == 0:
            self.param1 = 1
        self.update_image()

    def update_param2(self, value):
        self.param2 = value
        if self.param2 == 0:
            self.param2 = 1
        self.update_image()

    def update_iou(self, value):
        self.iou_border = value / 100
        self.update_image()

    """UPDATE MEDIAN BLUR"""

    def update_median_blur(self, value):
        self.median_blur = value
        if self.median_blur % 2 == 0:
            self.median_blur += 1
        self.update_image()

    """LOAD GUI"""

    def load_options(self):
        cv.createTrackbar("EQUAL", self.OPTIONS_WINDOW_NAME, self.equalization, 1, self.update_equalization)
        cv.createTrackbar("KERNEL", self.OPTIONS_WINDOW_NAME, self.kernel, 50, self.update_kernel)
        cv.createTrackbar("SIGMA", self.OPTIONS_WINDOW_NAME, self.sigma, 30, self.update_sigma)
        cv.createTrackbar("MEDIAN", self.OPTIONS_WINDOW_NAME, self.median_blur, 50, self.update_median_blur)
        cv.createTrackbar("CANNY ON", self.OPTIONS_WINDOW_NAME, self.canny_on, 1, self.update_canny)
        cv.createTrackbar("TRESHOLD", self.OPTIONS_WINDOW_NAME, self.canny_min, 200, self.update_treshold)
        cv.createTrackbar("BILATERAL", self.OPTIONS_WINDOW_NAME, self.is_bilateral_on, 1, self.update_is_bilateral_on)
        cv.createTrackbar("BIL. SIZE", self.OPTIONS_WINDOW_NAME, self.bilateral_filter_size, 5, self.update_bilateral_filter_size)
        cv.createTrackbar("BSIG COLOR", self.OPTIONS_WINDOW_NAME, self.bilateral_sigma_color, 300, self.update_bilateral_sigma_color)
        cv.createTrackbar("BSIG SPACE", self.OPTIONS_WINDOW_NAME, self.bilateral_sigma_space, 300, self.update_bilateral_sigma_space)
        cv.createTrackbar("HOUGH ON", self.OPTIONS_WINDOW_NAME, self.circles_on, 1, self.update_circles_on)
        cv.createTrackbar("ACCUMULATOR", self.OPTIONS_WINDOW_NAME, self.accumulator, 10, self.update_accumulator)
        cv.createTrackbar("MIN DIST", self.OPTIONS_WINDOW_NAME, self.min_dist, 80, self.update_min_dist)
        cv.createTrackbar("MIN RADIUS", self.OPTIONS_WINDOW_NAME, self.min_radius, 100, self.update_min_radius)
        cv.createTrackbar("MAX RADIUS", self.OPTIONS_WINDOW_NAME, self.max_radius, 250, self.update_max_radius)
        cv.createTrackbar("PARAM 1", self.OPTIONS_WINDOW_NAME, self.param1, 500, self.update_param1)
        cv.createTrackbar("PARAM 2", self.OPTIONS_WINDOW_NAME, self.param2, 300, self.update_param2)
        cv.createTrackbar("IOU", self.OPTIONS_WINDOW_NAME, int(self.iou_border * 100), 100, self.update_iou)


    def load_window(self):
        cv.namedWindow(self.IMAGE_WINDOW_NAME, cv.WND_PROP_FULLSCREEN)
        cv.namedWindow(self.OPTIONS_WINDOW_NAME, cv.WINDOW_NORMAL)
        image = deepcopy(self.original_image)
        self.update_image()
        cv.imshow(self.IMAGE_WINDOW_NAME, image)

    def load_image(self):
        file = filedialog.askopenfile(filetypes=[('Images', '*.jpg')], initialdir='./sources/001/L')
        if file is None:
            return
        self.original_image = cv.imread(cv.samples.findFile(file.name))
        self.load_csv_from_csv(cv.samples.findFile(file.name))

    def load_csv_from_csv(self, path):
        with open('./sources/iris_annotation.csv', newline='') as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                if path.endswith(row['image']):
                    self.csv_data = row
                    break
